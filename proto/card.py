# coding: utf-8

class Deck(object):
    def __init__(self, cards=None):
        self.cards = cards
    
    @staticmethod
    def load_json(filename):

        import json

        card_list = []

        with open(filename) as input:
            cards = json.load(input)

        for id in cards:
            id = id.replace("/", "_")
            with open('../dataset/details/%s.json' % id) as input:
                card_list.append(Card(**json.load(input)))

        return Deck(card_list)
        
class Card(object):
    def __init__(self, **kwargs):
        self.data = kwargs
        self.effects = []

        self._init_effect()

    def _init_effect(self):
        print self.data['text'].encode('utf-8')
