# coding: utf-8

class Effect(object):

    def __init__(self):
        self.type = None # 永続、起動、自動
        self.condition = None
        self.effects = None
        self.optional_commands = None

    def load(self, file): pass
    def check(self, game): pass
    def available_commands(self, game): pass

# 条件を満たすカードか
# 条件を満たす操作をしたか
# -> プレイヤーが行う操作以外の条件を満たしていれば、別メソッドに遷移
