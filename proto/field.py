# coding: utf-8

import logging
import random

class Field(object):

    STAGE = 'stage'
    CLIMAX = 'climax'
    STOCK = 'stock'
    LEVEL = 'level'
    CLOCK = 'clock'
    DECK = 'deck'
    DISCARD = 'discard'
    MEMORIAL = 'memorial'

    def __init__(self, deck):
        self.hand = SubField()
        self.stage = Stage()
        self.climax = SubField()
        self.stock = SubField()
        self.level = SubField()
        self.clock = SubField()
        self.deck = SubField()
        self.discard = SubField()
        self.memorial = SubField()

        self.deck.cards = deck.cards
        self.deck.shuffle()

        print len(self.deck.cards), self.deck.cards

class SubField(object):

    def __init__(self):
        self.cards = []

    def __getitem__(self, key):
        return self.cards[key]

    def __setitem__(self, key, val):
        self.cards[key] = val

    def __delitem__(self, key):
        del self.cards[key]
    
    def first(self):
        return self.cards[0]

    def all(self):
        return self.cards

    def remove(self): pass

    def clear(self):
        self.cards = []

    def shuffle(self):
        random.shuffle(self.cards)

class Stage(SubField): pass
