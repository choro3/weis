# coding: utf-8

class Player(object):
    def __init__(self):
        pass
        
    # 自ターンの各フェイズのイベントハンドラ
    def clock_phase_hook(self):
        raw_input("[clock-phase-hook] hit any key...")

    def main_phase_hook(self):
        raw_input("[main-phase-hook] hit any key...")

    def climax_phase_hook(self):
        raw_input("[climax-phase-hook] hit any key...")

    def attack_phase_hook(self):
        raw_input("[attack-phase-hook] hit any key...")

    def end_phase_hook(self):
        raw_input("[end-phase-hook] hit any key...")

    # 相手ターンでのカード効果によるカウンターなどのイベントハンドラ
    def counter_step_hook(self): pass
