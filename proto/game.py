# coding: utf-8

import logging

from field import *
from card import *
from effect import *
from player import *

logging.basicConfig(level=logging.DEBUG)

class Game(object):
    
    def __init__(self, player1, player2, deck1, deck2):
        self.players = (player1, player2)
        self.fields = (Field(deck1), Field(deck2))

        self.turn = 0

    def status(self):
        return "status: turn %d" % self.turn

    def log(self): pass

    def run(self):

        logging.debug('game start.')

        self._prepare()

        while not self._over():

            logging.debug(self.status())

            self._stand_phase()
            self._draw_phase()
            self._clock_phase()
            self._main_phase()
            self._climax_phase()
            self._attack_phase()
            self._end_phase()

            self.turn += 1

    def _over(self):
        return self.turn > 100

    # 各フェーズ単位での進行
    # Player イベントハンドラの呼び出し
    def _prepare(self):
        logging.debug("prepare")

    def _stand_phase(self):
        logging.debug("1. stand phase")

    def _draw_phase(self):
        logging.debug("2. draw phase")

    def _clock_phase(self):
        logging.debug("3. clock phase")

    def _main_phase(self):
        logging.debug("4. main phase")

    def _climax_phase(self):
        logging.debug("5. climax phase")

    def _attack_phase(self):
        logging.debug("6. attack phase")

    def _end_phase(self):
        logging.debug("7. end phase")

class Command(object):
    def __init__(self):
        pass

# 条件を表現できる適当な言語、Gameの各操作のタイミングで評価値を更新していく、条件が真になるときのCommand 
# 操作ログは何が原因でおこったかも持っておく必要がある
# directly user-hooked manip / caused by card effect card=(Card, pos on field, ,,) /
# カードを選ぶ => 効果を適用する (ステータス(パワー)増、移動、

def main():
    deck1 = Deck.load_json('zero.json')
    deck2 = Deck.load_json('zero.json')
    game = Game(Player(), Player(), deck1, deck2)
    game.run()
    
if __name__ == '__main__':
    main()
