# -*- coding: utf-8 -*-

from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/search')
def search():
    return "search"

@app.route('/deck')
def deck_list():
    return "deck"

@app.route('/deck/<deck_id>')
def detail(deck_id):
    return render_template('detail.html')

@app.route('/deck/<deck_id>/fork', methods=['POST'])
def fork(deck_id):
    return "fork"

@app.route('/edit')
def edit():
    return "edit"

@app.route('/card/<card_id>')
def card_detail(card_id):
    return card_id

@app.route('/card')
def card_list():
    return "card"

@app.route('/login', methods=['POST', 'GET'])
def login():
    return "login"

@app.route('/logout', methods=['POST', 'GET'])
def logout():
    return "logout"

@app.route('/my')
def account():
    return "my"

if __name__ == '__main__':
    app.run(debug=True)
