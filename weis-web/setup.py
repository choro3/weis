from setuptools import setup, find_packages
import sys, os

version = '0.0'

setup(name='weis-web',
      version=version,
      description="",
      long_description="""\
""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='',
      author='choro3',
      author_email='mail@choro3.net',
      url='http://www.choro3.net/',
      license='',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'flask',
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
