# -*- coding: utf-8 -*-

import os
import os.path
import json

from weis.recipe.database import db_session
from weis.recipe.models import Card

CARDDETAIL_DIR = '../../dataset/details'

def cards():
    for file in os.listdir(CARDDETAIL_DIR):
        with open(os.path.join(CARDDETAIL_DIR, file)) as input:
            card_data = json.load(input)
            card_data['card_no'] = card_data['card_id']
            del card_data['card_id']
            yield card_data

def main():

    Card.query.delete()

    for card in cards():
        card = Card(**card)
        db_session.add(card)

    db_session.commit()

if __name__ == '__main__':
    main()
