# -*- coding: utf-8 -*-

from flask import Flask, jsonify

from models import Card, Tag, DeckMeta, DeckCards, DeckTags

app = Flask(__name__)

STATUS_OK = 200

def build_response(status_code, body, headers=None):
    response = jsonify(body)
    response.status_code = status_code
    response.header = headers
    return response

######################################################################

@app.route('/cards', methods=['GET'])
def get_cards():
    return build_response(STATUS_OK, {})

@app.route('/cards/<card_id>', methods=['GET'])
def get_card(card_id):
    return build_response(STATUS_OK, {})

@app.route('/cards/<card_id>', methods=['PUT'])
def put_card(card_id):
    return build_response(STATUS_OK, {})

@app.route('/cards/<card_id>', methods=['DELETE'])
def delete_card(card_id):
    return build_response(STATUS_OK, {})

######################################################################

@app.route('/decks', methods=['GET'])
def get_decks():
    return build_response(STATUS_OK, {})

@app.route('/decks/<deck_id>', methods=['GET'])
def get_deck(deck_id):
    return build_response(STATUS_OK, {})

@app.route('/decks', methods=['POST'])
def post_deck():
    return build_response(STATUS_OK, {})

@app.route('/decks/<deck_id>', methods=['DELETE'])
def delete_deck():
    return build_response(STATUS_OK, {})

######################################################################

if __name__ == '__main__':
    app.run(debug=True)
