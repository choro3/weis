# -*- coding: utf-8 -*-

from sqlalchemy import Column, ForeignKey, VARCHAR, Integer, DATETIME, TEXT

from weis.recipe.database import Base

class Card(Base):

    __tablename__ = 'cards'
    
    id = Column('id', Integer, primary_key=True)
    card_no = Column('card_no', VARCHAR(32), unique=True)
    name = Column('name', VARCHAR(256), nullable=False)
    rare = Column('rare', VARCHAR(8))
    power = Column('power', Integer)
    level = Column('level', Integer)
    color = Column('color', VARCHAR(8), nullable=False)
    trigger  = Column('trigger', Integer, nullable=False)
    type = Column('type', VARCHAR(16), nullable=False)
    cost = Column('cost', Integer, nullable=False)
    name_ruby = Column('name_ruby', VARCHAR(256), nullable=False)
    text = Column('text', TEXT)
    flavor = Column('flavor', VARCHAR(256))
    side = Column('side', VARCHAR(8), nullable=False)

    def __init__(self,
                 card_no=None,
                 name=None,
                 rare=None,
                 power=None,
                 level=None,
                 color=None,
                 trigger=None,
                 type=None,
                 cost=None,
                 name_ruby=None,
                 text=None,
                 flavor=None,
                 side=None):
        self.card_no = card_no
        self.name = name
        self.rare = rare
        self.power = power
        self.level = level
        self.color = color
        self.trigger = trigger
        self.type = type
        self.cost = cost
        self.name_ruby = name_ruby
        self.text = text
        self.flavor = flavor
        self.side = side

    def __repr__(self):
        return '<Card %s>' % self.card_no

    @staticmethod
    def search(): pass

class Tag(Base):

    __tablename__ = 'tags'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', VARCHAR(128), nullable=False, unique=True)

    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        return '<tag %s>' % self.name

class DeckMeta(Base):

    __tablename__ = 'deck_meta'

    id = Column('id', Integer, primary_key=True)
    author = Column('author', VARCHAR(128))
    date = Column('date', DATETIME, nullable=False)
    comment = Column('comment', TEXT)

    def __init__(self, author=None, date=None, comment=None):
        self.author = author
        self.date = date
        self.comment = comment

    def __repr__(self):
        return '<Deck %s>' % self.id


class DeckCards(Base):

    __tablename__ = 'deck_cards'

    id = Column('id', Integer, primary_key=True)
    deck_id = Column('deck_id', Integer, ForeignKey('deck_meta.id'), nullable=False)
    card_id = Column('card_id', Integer, ForeignKey('cards.id'), nullable=False)
    size = Column('size', Integer)

    def __init__(self, deck_id=None, card_id=None, size=None):
        self.deck_id = deck_id
        self.card_id = card_id
        self.size = size

class DeckTags(Base):

    __tablename__ = 'deck_tags'

    id = Column('id', Integer, primary_key=True)
    deck_id = Column('deck_id', Integer, ForeignKey('deck_meta.id'), nullable=False)
    tag_id = Column('tag_id', Integer, ForeignKey('tags.id'), nullable=False)
    date = Column('date', DATETIME, nullable=False)

    def __init__(self, deck_id=None, tag_id=None, date=None):
        self.deck_id = deck_id
        self.tag_id = tag_id
        self.date = date

