# coding: utf-8

from pyquery import PyQuery as pq
import requests
import time
import json

def get_detail(card_id):
    res = requests.get('http://ws-tcg.com/jsp/cardlist?cardno=%s' % card_id)
    res.encoding = 'utf-8'
    return res.text

def parse(content):

    cells = pq(content)('#cardDetail > .status td')

    name, name_ruby = cells[1].text_content().strip().split('\r\n')
    card_id = cells[2].text_content().strip()
    rare = cells[3].text_content().strip()
    expansion = cells[4].text_content().strip()
    side = pq(cells[5])('img')[0].attrib['src'].split('/')[-1].split('.')[0]
    cardtype = cells[6].text_content().strip()
    color = pq(cells[7])('img')[0].attrib['src'].split('/')[-1].split('.')[0]
    level = cells[8].text_content().strip()
    cost =  cells[9].text_content().strip()
    power = cells[10].text_content().strip()
    soul = unicode(len(pq(cells[11])('img')))
    trigger = unicode(len(pq(cells[12])('img')))
    feature = cells[13].text_content().strip()
    text = cells[14].text_content().strip()
    flavor = cells[15].text_content().strip()

    return { 'name': name,
             'name_ruby': name_ruby,
             'card_id': card_id,
             'rare': rare,
             'expansion': expansion,
             'side': side,
             'cardtype': cardtype,
             'color': color,
             'level': level,
             'cost': cost,
             'power': power,
             'soul': soul,
             'trigger': trigger,
             'feature': feature,
             'text': text,
             'flavor': flavor
         }

def main():

    import csv

    with open('cardlist.csv') as input:
        reader = csv.reader(input)
        for row in reader:
            try:
                detail = parse(get_detail(row[0]))
                json.dump(detail, open('%s.json' % row[0].replace('/', '_'), 'w'))
            except KeyboardInterrupt:
                exit()
            except:
                print row[0]
            time.sleep(1)

if __name__ == '__main__':
    main()
