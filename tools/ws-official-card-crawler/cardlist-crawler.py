# coding: utf-8

from pyquery import PyQuery as pq
import requests
import time

def get_detail(expansion_id, page):
    res = requests.post('http://ws-tcg.com/jsp/cardlist/expansionDetail',
                        data={'expansion_id': expansion_id, 'page': page})
    res.encoding = 'utf-8'
    return res.text

def parse(content):
    query = pq(content)
    return [row.text_content().strip().replace('\r\n', '')
            for row in query('#expansionDetail_table > table > tr')]

def main():
    for ex_id in xrange(1, 200):
        for page in xrange(1, 60):
            content = get_detail(ex_id, page)
            if u'<p class="center">カードの登録はありません。</p>' in content: break
            print '\n'.join(parse(content)[1:]).encode('utf-8')
            time.sleep(1)

def prettify():

    import re

    with open('cardlist') as f, open('cardlist.csv', 'w') as out:
        for line in f:
            line = line.strip()
            if not line: continue
            l = re.split('\s{8}', line)
            out.write('"%s","%s","%s"\n' % (l[0], l[1], l[2]))

if __name__ == '__main__':
    # main()
    prettify()
